﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordMath.Tests.Unit
{

    public interface IConfigureWordMathTestStructure
    {
        void Setup();
        void Teardown();
        void Word_Datasource_Returns_All_Words();
        void Word_Service_Returns_Matching_Numerical_Value_100();
    }

}
