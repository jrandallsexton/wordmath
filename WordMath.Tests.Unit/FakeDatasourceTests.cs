﻿
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.Interfaces;

using NUnit.Framework;

namespace WordMath.Tests.Unit
{

    [TestFixture]
    public class FakeDatasourceTests : IConfigureWordMathTestStructure 
    {

        IWordMathService _service = null;
        IWordDataSource _datasource = null;
        IWordMathOptions _options = null;

        [TestFixtureSetUp]
        public void Setup()
        {
            this._datasource = new FakeDatasource();
            this._options = new WordMathOptions();
            this._service = new BasicService(this._datasource, this._options);
        }

        [Test]
        public void Word_Datasource_Returns_All_Words()
        {
            IEnumerable<string> words = this._service.GetWords();
            Assert.AreEqual(4, words.ToList<string>().Count);
        }

        [Test]
        public void Word_Service_Returns_Matching_Numerical_Value_19()
        {
            IEnumerable<string> words = this._service.GetWords(19);
            Assert.AreEqual(1, words.ToList<string>().Count);
            Assert.AreEqual("and", words.ToList<string>()[0]);
        }

        [Test]
        public void Word_Service_Returns_Matching_Numerical_Value_100()
        {
            IEnumerable<string> words = this._service.GetWords(100);
            Assert.AreEqual(1, words.ToList<string>().Count);
            Assert.AreEqual("goloshes", words.ToList<string>()[0]);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            this._datasource = null;
            this._service = null;
        }

    }

}
