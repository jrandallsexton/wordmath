﻿
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.Interfaces;
using WordMath.Data.Text;

using NUnit.Framework;

namespace WordMath.Tests.Unit
{

    [TestFixture]
    public class TextDatasourceTests : IConfigureWordMathTestStructure 
    {

        IWordMathService _service = null;
        IWordDataSource _datasource = null;
        IWordMathOptions _options = null;

        [TestFixtureSetUp]
        public void Setup()
        {
            this._datasource = new WordMathTextDatasource();
            this._options = new WordMathOptions();
            this._service = new BasicService(this._datasource, this._options);
        }
        
        [Test]
        public void Word_Datasource_Returns_All_Words()
        {
            IEnumerable<string> words = this._service.GetWords();
            Assert.AreEqual(4610, words.ToList<string>().Count);
        }

        [Test]
        public void Word_Service_Returns_Matching_Numerical_Value_100()
        {
            IEnumerable<string> words = this._service.GetWords(100);
            Assert.AreEqual(35, words.ToList<string>().Count);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            this._datasource = null;
            this._service = null;
        }

    }

}
