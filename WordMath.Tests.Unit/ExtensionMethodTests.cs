﻿
using WordMath.Common.ExtensionMethods;
using WordMath.Common.Interfaces;

using NUnit.Framework;

namespace WordMath.Tests.Unit
{
    
    [TestFixture]
    public class ExtensionMethodTests    
    {

        IWordMathOptions _options = null;

        [TestFixtureSetUp]
        public void Setup()
        {
            this._options = new WordMathOptions();
        }

        [Test]
        public void Word_Value_Matches_Austin_Uppercase()
        {
            string test = "AUSTIN";
            Assert.AreEqual(84, test.WordValue(_options));
        }

        [Test]
        public void Word_Value_Matches_Austin_Lowercase()
        {
            string test = "austin";
            Assert.AreEqual(84, test.WordValue(_options));
        }

        [Test]
        public void Word_Value_Matches_Austin_Two_Times_Ignores_Spacing()
        {
            string test = "austin austin";
            Assert.AreEqual(168, test.WordValue(_options));
        }

        [Test]
        public void Word_Value_Matches_Austin_Two_Times_Ignores_Garbage()
        {
            string test = "austin 1 ~@#$ austin @#$$#^$%^";
            Assert.AreEqual(168, test.WordValue(_options));
        }

        [Test]
        public void Char_Value_Returns_1_For_Uppercase_A()
        {
            char c = 'A';
            Assert.AreEqual(1, c.LetterValue(_options));
        }

        [Test]
        public void Char_Value_Returns_1_For_Lowercase_A()
        {
            char c = 'a';
            Assert.AreEqual(1, c.LetterValue(_options));
        }

        [Test]
        public void Char_Value_Returns_0_For_Space()
        {
            char c = ' ';
            Assert.AreEqual(0, c.LetterValue(_options));
        }

        [Test]
        public void Char_Value_Returns_0_For_Punctuation_Quote()
        {
            char c = '"';
            Assert.AreEqual(0, c.LetterValue(_options));
        }

        [Test]
        public void Char_Value_Returns_0_For_Punctuation_Apostrophe()
        {
            char c = '\'';
            Assert.AreEqual(0, c.LetterValue(_options));
        }

        [Test]
        public void Char_Value_Returns_0_For_Punctuation_Misc()
        {
            char c = '.';
            Assert.AreEqual(0, c.LetterValue(_options));
        }

    }

}
