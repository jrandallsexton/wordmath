﻿
using System;
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.Interfaces;

namespace WordMath.Tests.Unit
{

    public class FakeDatasource : IWordDataSource 
    {

        private List<string> _values = null;

        public FakeDatasource()
        {
            this._values = new List<string>();
            this._values.Add("and");    // => 1+14+4 = 19
            this._values.Add("austin"); // => 1+21+19+20+9+14=84
            this._values.Add("chattanooga"); // => 3+8+1+20+20+1+14+15+15+7+1 = 105
            this._values.Add("goloshes"); // => 7+15+12+15+19+8+5+19
        }

        public IQueryable<IWord> Words
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<string> GetWords()
        {
            return this._values;
        }

        public void SaveWord(IWord word)
        {
            throw new NotImplementedException();
        }
    }

}
