﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WordMath;
using WordMath.Common.Interfaces;
using WordMath.Data;

namespace WordMath.Utils.cs
{

    public class DataLoader
    {

        // having to provide WordMathOptions to simply load/save data is "smelly"
        private IWordMathService service = new BasicService(new WordMathSqlDb(), new WordMathOptions());

        public void LoadDataIntoSqlFromTextFile()
        {
            var fileLocation = @"~\..\..\..\..\Data\american-words.txt";

            using (StreamReader reader = new StreamReader(fileLocation))
            {
                while (!reader.EndOfStream)
                {
                    service.SaveWord(new Word() { Value = reader.ReadLine() });
                }
            }

        }

    }

}