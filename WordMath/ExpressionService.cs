﻿
using System;
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.ExtensionMethods;
using WordMath.Common.Interfaces;

namespace WordMath
{
    
    public class ExpressionService : IWordMathService 
    {

        IWordMathOptions _options = null;
        IWordDataSource _dataSource = null;

        public ExpressionService(IWordDataSource dataSource, IWordMathOptions options)
        {
            this._dataSource = dataSource;
            this._options = options;
        }

        public IEnumerable<string> GetWords()
        {
            return this._dataSource.GetWords();
        }

        public void SaveWord(IWord word)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetWords(int wordValue)
        {
            return this.GetWords().Where<string>(x => x.WordValue(_options) == wordValue);
        }

    }

}
