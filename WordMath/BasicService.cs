﻿
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.ExtensionMethods;
using WordMath.Common.Interfaces;

namespace WordMath
{

    public class BasicService : IWordMathService
    {        

        IWordMathOptions _options = null;
        IWordDataSource _dataSource = null;

        public BasicService(IWordDataSource dataSource, IWordMathOptions options)
        {
            this._dataSource = dataSource;
            this._options = options;
        }

        public IEnumerable<string> GetWords()
        {
            return this._dataSource.GetWords();                
        }

        public void SaveWord(IWord word) {
            this._dataSource.SaveWord(word);
        }

        public IEnumerable<string> GetWords(int wordValue)
        {

            IEnumerable<string> words = this.GetWords();
            
            return
                from w in words
                where w.WordValue(this._options) == wordValue
                orderby w
                select w;

        }

    }

}