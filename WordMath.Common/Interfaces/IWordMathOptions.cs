﻿
namespace WordMath.Common.Interfaces
{

    public interface IWordMathOptions
    {
        int OffsetUpper { get; }
        int OffsetLower { get; }
        int MinUpper { get; }
        int MaxUpper { get; }
        int MinLower { get; }
        int MaxLower { get; }
        ECasing Casing { get; }
    }

}