﻿
using System.Collections.Generic;
using System.Linq;

namespace WordMath.Common.Interfaces
{

    public interface IWordDataSource
    {
        IQueryable<IWord> Words { get; }
        IEnumerable<string> GetWords();
        void SaveWord(IWord word);
    }
}
