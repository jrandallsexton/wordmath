﻿
namespace WordMath.Common.Interfaces
{
    public interface IWord
    {
        int Id { get; set; }
        string Value { get; set; }
    }
}
