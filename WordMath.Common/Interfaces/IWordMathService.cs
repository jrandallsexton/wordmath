﻿
using System.Collections.Generic;

namespace WordMath.Common.Interfaces
{

    public interface IWordMathService
    {
        IEnumerable<string> GetWords();
        void SaveWord(IWord word);
        IEnumerable<string> GetWords(int wordValue);
    }

}