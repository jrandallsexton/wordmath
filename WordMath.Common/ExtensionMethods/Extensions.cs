﻿
using System;
using System.Linq;

using WordMath.Common.Interfaces;

namespace WordMath.Common.ExtensionMethods
{

    public static class Extensions
    {

        //public static int WordValue(this string value, IWordMathOptions options)
        //{
        //    char[] chars = value.ToCharArray();

        //    int sum = 0;
        //    foreach (char c in chars) { sum += c.LetterValue(options); }

        //    return sum;
        //}

        public static int WordValue(this string value, IWordMathOptions options)
        {
            return value.ToCharArray().Sum(c => c.LetterValue(options));
        }

    }

    public static class CharExtensions
    {

        public static int LetterValue(this Char value, IWordMathOptions options)
        {
            if (string.IsNullOrEmpty(value.ToString())) { return 0; }

            int returnValue = (int)value;

            if (((returnValue < options.MinUpper) || (returnValue > options.MaxLower)) ||
                ((returnValue > options.MaxUpper) && (returnValue < options.MinLower)))
                { return 0; }

            return (Char.IsUpper(value)) ? returnValue - options.OffsetUpper : returnValue - options.OffsetLower;
            
        }

    }

}