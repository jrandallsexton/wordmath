﻿namespace WordMath.Common
{
    public enum ECasing
    {
        None = 0,
        Lower = 1,
        Upper = 2
    }
}