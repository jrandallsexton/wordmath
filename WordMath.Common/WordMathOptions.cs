﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WordMath.Common;
using WordMath.Common.Interfaces;

namespace WordMath
{

    public class WordMathOptions : IWordMathOptions
    {
        public int OffsetUpper { get { return 64; } }
        public int OffsetLower { get { return 96; } }
        public int MinUpper { get { return 65; } }
        public int MaxUpper { get { return 90; } }
        public int MinLower { get { return 97; } }
        public int MaxLower { get { return 122; } }
        public ECasing Casing { get { return ECasing.None; } }
    }

}
