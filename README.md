Dollar Words:

The input file american-words.txt is attached.

Using a C# console application, give each letter in each word a numerical value equal to its position in the alphabet.  So "a" is 1 and "z" is 26.  Punctuation and whitespace is 0.  Find words where the sum of the value of the letters is 100 and print each of the “100” words on the console.

Don't forget:  Use Linq2Sql for data access.