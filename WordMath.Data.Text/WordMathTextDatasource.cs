﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

using WordMath.Common.Interfaces;

namespace WordMath.Data.Text
{

    public class WordMathTextDatasource : IWordDataSource 
    {

        private string _filelocation = @"\american-words.txt";

        public WordMathTextDatasource()
        {
            this._filelocation = ConfigurationManager.AppSettings["wordFileLocation"];
            if (string.IsNullOrEmpty(this._filelocation)) { throw new SystemException("Value for wordFileLocation not found in app.config"); }
        }

        public IQueryable<IWord> Words
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerable<string> GetWords()
        {

            List<string> words = new List<string>();

            using (StreamReader rdr = new StreamReader(_filelocation))
            {
                while (!rdr.EndOfStream)
                {
                    words.Add(rdr.ReadLine().Trim());
                }

            }

            return words;
        }

        public void SaveWord(IWord word)
        {
            throw new NotImplementedException();
        }

    }

}
