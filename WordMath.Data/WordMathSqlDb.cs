﻿
using System;
using System.Collections.Generic;
using System.Linq;

using WordMath.Common.Interfaces;

namespace WordMath.Data
{

    public class WordMathSqlDb : IWordDataSource
    {

        WMDataClassesDataContext _context = new WMDataClassesDataContext();

        public IEnumerable<string> GetWords()
        {

            return
                from w in _context.Words
                orderby w.Value
                select w.Value;

        }

        public IQueryable<IWord> Words
        {
            get { throw new NotImplementedException(); }
        }

        public void SaveWord(IWord word)
        {
            _context.Words.InsertOnSubmit((Word)word);
            _context.SubmitChanges();
        }

    }

}
