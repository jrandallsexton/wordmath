﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WordMath;
using WordMath.Common.Interfaces;
using WordMath.Data;
using WordMath.Data.Text;

namespace DollarWords
{

    class Program
    {        

        static void Main(string[] args)
        {

            /* NOTE */
            // This could very easily be swapped out for a factory method which would
            // use the app.config to specify the repository and
            // load it at runtime using reflection

            IWordDataSource _datasource = new WordMathSqlDb();
            //IWordDataSource _datasource = new WordMathTextDatasource();

            IWordMathOptions _options = new WordMathOptions();

            //IWordMathService _service = new BasicService(_datasource, _options);
            IWordMathService _service = new ExpressionService(_datasource, _options);

            Console.Write("Enter the numerical value to find: ");
            int value = int.Parse(Console.ReadLine());
            int idx = 0;

            Console.WriteLine();
            foreach (var word in _service.GetWords(value))
            {
                Console.WriteLine(word);
                idx++;
            }

            Console.WriteLine("\n{0} words found with the value of {1}", idx, value);
            Console.WriteLine("\nPress any key to exit");
            Console.ReadKey();
        }

    }

}
